# Le language "Python"

## Définition

!!!tldr "Définition"
    Le Python est un langage de programmation interprété, multi-paradigme et multiplateformes. Il favorise la programmation impérative structurée, fonctionnelle et orientée objet. Il est doté d'un typage dynamique fort, d'une gestion automatique de la mémoire par ramasse-miettes et d'un système de gestion d'exceptions.
    __*source: Wikipedia[^1]*__
    [^1]: [Wikipedia : Definition](https://fr.wikipedia.org/wiki/Python_(langage))

- [X] Orienté Fonctionelle    
- [X] Orientée Objet



```lua linenums="1"
--8<-- "docs/scripts/python.py"
```

